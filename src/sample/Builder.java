package sample;

import java.io.*;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;

public final class Builder {
    private final static String _logFileName = "out.log";

    public static void buildingODbyANT(String fileName) throws FileNotFoundException, BuildException {
        Project project = new Project();
        try {
            DefaultLogger logger = getLogger(new PrintStream(_logFileName));
            project.addBuildListener(logger);
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException(e.getMessage());
        }

        File buildFile = new File(fileName);
        project.setUserProperty("ant.file", buildFile.getAbsolutePath());

        try {
            project.fireBuildStarted();
            project.init();
            ProjectHelper projectHelper = ProjectHelper.getProjectHelper();
            project.addReference("ant.projectHelper", projectHelper);
            projectHelper.parse(project, buildFile);
            project.executeTarget(project.getDefaultTarget());
            project.fireBuildFinished(null);
        } catch (BuildException buildException) {
            project.fireBuildFinished(buildException);
            throw new BuildException("Build Failed", buildException);
        }
    }

    private static DefaultLogger getLogger(PrintStream out) {
        DefaultLogger consoleLogger = new DefaultLogger();
        consoleLogger.setErrorPrintStream(out);
        consoleLogger.setOutputPrintStream(out);
        consoleLogger.setMessageOutputLevel(Project.MSG_INFO);

        return consoleLogger;
    }

    public static String buildingODbyCMD(String fileName) throws IOException {
        String success = "false";
        File buildFile = new File(fileName);
        if (!buildFile.exists())
            throw new IOException("Wrong path to files.");

        ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", "cd " + fileName + " && ant");
        builder.redirectErrorStream(true);
        Process process = builder.start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        BufferedWriter writer = new BufferedWriter(new FileWriter(_logFileName));
        String line;
        while (true) {
            line = reader.readLine();
            if (line == null) break;
            if (line.contains("BUILD SUCCESSFUL")) success = "true";
            writer.write(line + "\n");
        }
        writer.close();

        return success;
    }
}
