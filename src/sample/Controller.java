package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.awt.*;
import java.io.*;


public class Controller {
    private Stage _primaryStage;
    private Props _props;
    private String _logFileName;
    private boolean _ant;

    private ProcessingTask worker;

    @FXML private TextField textField;

    @FXML private HBox container;

    @FXML private Label label;
    @FXML private Label message;

    @FXML private Button close;
    @FXML private Button searchFolder;
    @FXML private Button start;
    @FXML private Button linkLogFile;
    @FXML private Button changeBuilder;

    @FXML private CheckBox firstTime;
    @FXML private CheckBox runOD;

    @FXML private ProgressBar progressBar;

    private void setText(String text) {
        textField.setText(text);
    }

    private void setStyleClass(String className) {
        resetStyleClass();
        container.getStyleClass().add(className);
        label.getStyleClass().add(className);
        progressBar.getStyleClass().add("progress-bar");
        progressBar.getStyleClass().add(className);
    }

    private void resetStyleClass() {
        container.getStyleClass().clear();
        label.getStyleClass().clear();
        progressBar.getStyleClass().clear();
    }

    private void setMessage(String state) {
        String text;
        String className;

        if ("processing".equals(state)) {
            text = "Processing ...";
            className = "labelProcessing";
        } else if ("Success".equals(state)) {
            text = "Success";
            className = "labelDone";
        } else {
            text = state;
            className = "labelError";
        }

        message.getStyleClass().clear();
        message.setText(text);
        message.getStyleClass().add(className);
        message.setVisible(true);
    }

    private void focusState(Boolean value) {
        if (value)
            setStyleClass("focus");
        else
            resetStyleClass();
    }

    private void startWorker() {
        worker = new ProcessingTask();
        {
            String ff = getFullPath(_props.getProperty("firstBuildFile"));
            String sf = getFullPath(_props.getProperty("secondBuildFile"));
            String fs = _props.getProperty("stringInFirstFile");
            String ss = _props.getProperty("stringInSecondFile");
            String ns = _props.getProperty("newText");
            worker.setForChanging(ff, sf, fs, ss, ns);
        }
        {
            String zip = getFullPath(_props.getProperty("dirWithZip") + _props.getProperty("ZipFile"));
            String dirZip = getFullPath(_props.getProperty("dirWithZip"));
            worker.setForUnzipping(zip, dirZip);
        }
        {
            String app = getFullPath(_props.getProperty("dirWithApp") + _props.getProperty("App"));
            String dirApp = getFullPath(_props.getProperty("dirWithApp"));
            worker.setForRunning(app, dirApp);
        }
        {
            String mainProp = _ant ? "mainBuildFile" : "mainBuildDir";
            worker.setForBuilding(getFullPath(_props.getProperty(mainProp)));
            worker.setBuilder(_ant);
        }

        worker.setChanged(firstTime.isSelected());
        worker.setRun(runOD.isSelected());

        progressBar.progressProperty().bind(worker.progressProperty());
        ChangeListener<String> listener = (observable, oldValue, newValue) -> getMessage(newValue);
        worker.valueProperty().addListener(listener);
        new Thread(worker).start();
    }

    @FXML private void click() {
        try {
            firstTime.setSelected(true);
            DirectoryChooser dirChooser = new DirectoryChooser();
            setText(dirChooser.showDialog(_primaryStage).getAbsolutePath());
        } catch (NullPointerException e) {
            System.out.println("Sad ...(");
        }
    }

    @FXML private void openLog() {
        try {
            Desktop.getDesktop().open(new File(_logFileName));
        } catch (IOException ignored) {}
    }

    @FXML private void start() {
        try {
            linkLogFile.setVisible(false);
            changePathToOD();
            setMessage("processing");
            startWorker();
            setStyleClass("focus");
        } catch (Exception ignored) {
            setStyleClass("incorrect");
        }
    }

    @FXML private void close() {
        try {
            _props.setProperty("pathToOD", _props.getProperty("pathToOD"));
            _props.setProperty("needToChangeBuildFiles", _props.getProperty("needToChangeBuildFiles"));
            _props.setProperty("buildBy", _ant ? "ant" : "cmd");
            _props.saveProperty();
        } catch (IOException ignored) {}
        finally {
            if (worker != null) worker.cancel();
            _primaryStage.close();
        }
    }

    @FXML private void changeBuilder() {
        _ant = !_ant;
        getBuilder();
    }

    private void getBuilder() {
        String tmp = _ant ? "> ANT" : "> CMD";
        changeBuilder.setText(tmp);
    }

    public void init(Stage stage) {
        _primaryStage = stage;

        textField.setFocusTraversable(false);
        runOD.setFocusTraversable(false);
        firstTime.setFocusTraversable(false);
        linkLogFile.setFocusTraversable(false);
        close.setFocusTraversable(false);
        textField.focusedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue)
                        -> focusState(newValue));
        linkLogFile.setVisible(false);
        message.setVisible(false);

        try {
            _props = new Props();
            if (!_props.getProperty("pathToOD").equals(""))
                textField.setText(_props.getProperty("pathToOD"));

            firstTime.setSelected(_props.getProperty("needToChangeBuildFiles").equals("true"));
            _ant = _props.getProperty("buildBy").equals("ant");
            getBuilder();
            runOD.setSelected(true);
        } catch (Exception ignored) {
            setStyleClass("incorrect");
            setMessage("Invalid configuration settings");
            start.setDisable(true);
            searchFolder.setDisable(true);
        }
    }

    private void changePathToOD() {
        String tmp = textField.getText();
        String path = tmp.endsWith("\\") ? tmp : tmp + "\\";
        if (!_props.getProperty("pathToOD").equals(path))
            _props.setProperty("pathToOD", path);
    }

    private void getMessage(String mes) {
        if ("changing".equals(mes)) {
            _props.setProperty("needToChangeBuildFiles", "false");
            firstTime.setSelected(false);
        } else if ("building".equals(mes)) {
            _logFileName = "out.log";
            linkLogFile.setVisible(true);
        } else if ("Success".equals(mes)) {
            setStyleClass("correct");
            setMessage(mes);
        } else {
            setStyleClass("incorrect");
            setMessage(mes);
        }
    }

    private String getFullPath(String name) {
        return _props.getProperty("pathToOD") + name;
    }
}
