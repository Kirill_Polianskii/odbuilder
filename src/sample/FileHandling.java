package sample;

import java.io.*;

public final class FileHandling {

    public static void handle(String fileName, String searchedLine, String newLine) throws IOException {
        findAndChange(fileName, searchedLine, newLine);
    }

    private static String log() {
        String fullClassName = Thread.currentThread().getStackTrace()[2].getClassName();
        String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();

        return "line " + lineNumber + ": " + className + "." + methodName + "():";
    }

    private static void findAndChange(String fileName, String searchedLine, String newLine) throws IOException {
        File buildFile = new File(fileName);
        if (!buildFile.exists())
            throw new IOException(log() + " Wrong path to files.");

        File temp = File.createTempFile("temporary", ".xml", buildFile.getParentFile());
        String charset = "UTF-8";
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(buildFile), charset));
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(temp), charset));
        for (String line; (line = reader.readLine()) != null;) {
            line = line.replace(searchedLine, newLine);
            writer.println(line);
        }
        reader.close();
        writer.close();
        buildFile.delete();
        temp.renameTo(buildFile);
    }
}
