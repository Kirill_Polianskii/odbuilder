package sample;

import javafx.concurrent.Task;
import org.apache.tools.ant.BuildException;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class ProcessingTask extends Task<String> {
    private final static double _maxProgress = 100.0;

    // For changeBuildFiles
    private String _firstFile;
    private String _secondFile;
    private String _firstString;
    private String _secondString;
    private String _newString;
    private boolean _changed;
    private boolean _ant;

    // For buildingOD
    private String _dir;

    // For openOrRunApp
    boolean _run;
    private String _appPath;
    private String _dirWithPath;

    // For unzip
    private String _zipFile;
    private String _dirWithZip;

    @Override
    public String call() {
        String state = "Success";
        try {
            resetProgress();
            if (_changed)
                changeBuildFiles();
            updateProgress(20, _maxProgress);

            buildingOD();
            updateProgress(80, _maxProgress);

            openOrRunApp();
            updateProgress(100, _maxProgress);
        } catch  (IOException | InterruptedException | BuildException exception) {
            state = exception.getMessage();
        }
        return state;
    }

    public void setChanged(boolean change) { _changed = change; }

    public void setForChanging(String firstF, String secondF, String firstS, String secondS, String newS) {
        _firstFile = firstF;
        _secondFile = secondF;
        _firstString = firstS;
        _secondString = secondS;
        _newString = newS;
    }

    public void setForBuilding(String dir) { _dir = dir; }

    public void setRun(boolean run) { _run = run; }

    public void resetProgress() { updateProgress(0, _maxProgress); }

    public void setForRunning(String app, String dirApp) {
        _appPath = app;
        _dirWithPath = dirApp;
    }

    public void setForUnzipping(String zip, String dirZip) {
        _zipFile = zip;
        _dirWithZip = dirZip;
    }

    public void setBuilder(boolean ant) { _ant = ant; }

    private void changeBuildFiles() throws IOException, InterruptedException {
        try {
            FileHandling.handle(_firstFile, _firstString, "");
            FileHandling.handle(_secondFile, _secondString, _newString);
            updateValue("changing");
        } catch (IOException e) {
            throw new IOException("Fail changing build files");
        }
    }

    private void buildingOD() throws IOException, BuildException {
        try {
            if (_ant) {
                Builder.buildingODbyANT(_dir);
            } else {
                if (Builder.buildingODbyCMD(_dir).equals("false")) {
                    updateValue("building");
                    throw new IOException();
                }
            }
            updateValue("building");
        } catch (IOException e) {
            throw new IOException("Build Failed");
        } catch (BuildException e) {
            updateValue("building");
            throw new BuildException(e.getMessage());
        }
    }

    private void openOrRunApp() throws IOException {
        String exePath;
        exePath = _run ? _appPath :_dirWithPath;
        try {
            unzip();
            Desktop.getDesktop().open(new File(exePath));
        } catch (IOException e) {
            throw new IOException("Fail unzipping or opening app");
        }
    }

    private void unzip() throws IOException {
        Unzip.unzip(_zipFile, _dirWithZip);
    }

}
