package sample;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;

public final class Props {
    public static final String[] propNames = {"firstBuildFile", "secondBuildFile", "stringInFirstFile",
            "stringInSecondFile", "newText", "pathToOD", "needToChangeBuildFiles", "dirWithApp",
            "dirWithZip", "ZipFile", "App", "mainBuildFile", "mainBuildDir", "buildBy"};

    private static final String pathToProperties = "src/sample/resources/config.properties";
    private final Hashtable<String, String> fileNames;
    private final Properties properties;

    public Props() throws IOException{
        FileInputStream stream = new FileInputStream(pathToProperties);
        properties = new Properties();
        properties.load(stream);

        fileNames = new Hashtable<>();
        for (String name: propNames)
            fileNames.put(name, properties.getProperty(name));
    }

    public String getProperty(String nameProp) {
        return fileNames.get(nameProp);
    }
    public void setProperty(String name, String value) {
        fileNames.put(name, value);
        properties.setProperty(name, value);
    }
    public void saveProperty() throws IOException {
        properties.store(new FileOutputStream(pathToProperties), null);
    }
}
