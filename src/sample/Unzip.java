package sample;

import net.lingala.zip4j.ZipFile;

import java.io.File;
import java.io.IOException;

public final class Unzip {
    public static void unzip(String zipName, String targetDir) throws IOException {
        new ZipFile(new File(zipName)).extractAll(targetDir);
    }
}
